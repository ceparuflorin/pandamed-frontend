import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

import { environment } from '../../environments/environment';
import { User } from '@app/_models/user';
import { map } from 'rxjs/internal/operators/map';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { AppointmentService } from './appointment.service';
import moment from 'moment';

const BACKEND_URL = environment.apiUrl + '/user/';

@Injectable({ providedIn: 'root' })

export class AccountService {

  private profileInfo: User[] = [];
  private profileUpdate = new Subject<User[]>();

  constructor(private http: HttpClient, private authService: AuthService, private router: Router, private appointmentService: AppointmentService) {}

  getProfileUpdateListener() {
    return this.profileUpdate.asObservable();
  }

  getProfileInfo(userId: string) {
    console.log("am plecat din get profile");
    this.http.get<{message: string, profileInfo: any}>(BACKEND_URL + 'profile-info/' + userId)
    .pipe(map((profileData) => {
      //console.log(profileData.message);
      return profileData.profileInfo.map(data => {
        return {
          firstName: data.firstName,
          lastName: data.lastName,
          cnp: data.cnp,
          birthDate: moment(data.birthDate).format('Do MMMM YYYY'),
          email: data.email
        }
      })
    }))
    .subscribe((profile) => {
      this.profileInfo = profile;
      this.profileUpdate.next([...this.profileInfo]);
    });
  }

  updateProfileInfo(userId: string, firstName: string, lastName: string) {
    this.http.patch(BACKEND_URL + 'update-profile/' + userId, {firstName: firstName, lastName: lastName})
      .subscribe(() => {
      });
  }

  updateEmailAdress(userId: string, email: string) {
    this.http.patch(BACKEND_URL + 'update-email/' + userId, {email: email})
      .subscribe(() => {
      });
  }

  deleteUser(userId: string) {
    this.http.delete(BACKEND_URL + 'delete-user/' + userId)
    .subscribe(() => {
      this.authService.logout()
    });
  }

  grandMedicAccess(userId: string, cnp: number) {
    this.http.patch<{message: string}>(BACKEND_URL + 'grand-access/' + userId, {cnp: cnp})
      .subscribe((response) => {
        console.log(response.message);
      });
  }
}
