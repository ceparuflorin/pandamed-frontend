import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Appointment } from '../_models/appointment';
import moment from 'moment';
import { AuthService } from './auth.service';

import { environment } from '../../environments/environment';

const BACKEND_URL = environment.apiUrl + '/appointments/';

@Injectable({providedIn: 'root'})
export class AppointmentService {
  private appointments: Appointment[] = [];
  private medAppointments: Appointment[] = [];
  private appointmentsUpdated = new Subject<Appointment[]>();
  private medAppointmentsUpdated = new Subject<Appointment[]>();
  updatedAppointment: Appointment[] = [];
  updatedMedAppointment: Appointment[] = [];

  constructor(private http: HttpClient, private authService: AuthService) {}

  getAppointments() {
    this.http.get<{message: string, appointments: any}>(BACKEND_URL)
      .pipe(map((appointmentData) => {
        console.log(appointmentData.message);

        return appointmentData.appointments.map(appointment => {
          var currentDate: Date = new Date(Date.now());
          var formatAppointmentDate = moment(appointment.appointmentDate).format('llll');
          var formatCurrentDate = moment(currentDate).format('llll');
          return {
            hospital: appointment.hospital,
            department: appointment.department,
            medicId: appointment.medicId,
            appointmentDate: appointment.appointmentDate,
            additionalDetails: appointment.additionalDetails,
            id: appointment._id,
            patientId: appointment.patientId,
            appointmentStatus: (moment(formatAppointmentDate).isSameOrBefore(formatCurrentDate)) ? 'Missed' : appointment.appointmentStatus
          }
        })
      }))
      .subscribe((transformedAppointment) => {
        this.appointments = transformedAppointment;
        this.appointmentsUpdated.next([...this.appointments]);
      });
  }

  getMedAppointments() {
    this.http.get<{message: string, appointments: any}>(BACKEND_URL + 'medapps/')
      .pipe(map((appointmentData) => {
        console.log(appointmentData.message);
        return appointmentData.appointments.map(appointment => {
          var currentDate: Date = new Date(Date.now());
          var formatAppointmentDate = moment(appointment.appointmentDate).format('llll');
          var formatCurrentDate = moment(currentDate).format('llll');
          return {
            hospital: appointment.hospital,
            department: appointment.department,
            medicId: appointment.medicId,
            appointmentDate: appointment.appointmentDate,
            additionalDetails: appointment.additionalDetails,
            id: appointment._id,
            patientId: appointment.patientId,
            appointmentStatus: (moment(formatAppointmentDate).isSameOrBefore(formatCurrentDate)) ? 'Missed' : appointment.appointmentStatus
          }
        })
      }))
      .subscribe((transformedAppointment) => {
        this.medAppointments = transformedAppointment;
        this.medAppointmentsUpdated.next([...this.medAppointments]);
      });
  }

  getOneAppointment(id: string) {
    this.http.get<{message: string, appointment: any}>(BACKEND_URL + id)
    .pipe(map((appointmentData) => {
      console.log(appointmentData.message);
      return appointmentData.appointment.map(appointment => {
        return {
          hospital: appointment.hospital,
          department: appointment.department,
          medicId: appointment.medicId,
          appointmentDate: appointment.appointmentDate,
          additionalDetails: appointment.additionalDetails,
          id: appointment._id,
          patientId: appointment.patientId,
          appointmentStatus: appointment.appointmentStatus
        }
      })
    }))
    .subscribe((transformedAppointment) => {
      this.updatedAppointment = transformedAppointment;
      const updatedAppointments = [...this.appointments];
      const oldIndex = updatedAppointments.findIndex(ap => ap.id === id);
      updatedAppointments[oldIndex] = this.updatedAppointment[0];
      this.appointments = updatedAppointments;
      this.appointmentsUpdated.next([...this.appointments]);
    });
  }

  getAppointmentUpdateListener() {
    return this.appointmentsUpdated.asObservable();
  }

  getMedAppointmentUpdateListener() {
    return this.medAppointmentsUpdated.asObservable();
  }

  addAppointments(hospital: string, department: string, medicId: string, date: Date, details: string) {
    const appointment: Appointment = {
      id: null,
      hospital: hospital,
      department: department,
      medicId: medicId,
      appointmentDate: date,
      additionalDetails: details,
      patientId: null,
      appointmentStatus: 'Pending'
    };
    this.http.post<{message: string, appointmentId: string}>(BACKEND_URL, appointment)
      .subscribe((responseData) => {
        console.log(responseData.message);
        const id = responseData.appointmentId;
        appointment.id = id;
        this.appointments.push(appointment);
        this.appointmentsUpdated.next([...this.appointments]);
      })
  }

  rescheduleAppointment(appointmentId: string, rescheduleDate: Date) {
    this.http.patch(BACKEND_URL + appointmentId, {date: rescheduleDate})
      .subscribe(() => {
        this.getOneAppointment(appointmentId);
      });
  }

  confirmAppointment(appointmentId: string) {
    this.http.patch(BACKEND_URL + 'confirm-appointment/' + appointmentId, {appointmentStatus: 'Confirmed'} )
    .subscribe(() => {
      this.getOneAppointment(appointmentId);
    });
  }

  cancelAppointment(appointmentId: string) {
    this.http.patch(BACKEND_URL + 'cancel-appointment/' + appointmentId, {appointmentStatus: 'Cancelled'} )
    .subscribe(() => {
      this.getOneAppointment(appointmentId);
    });
    window.location.reload();
  }

  deleteAppointment(appointmentId: string) {
    this.http.delete<{message: string}>(BACKEND_URL + appointmentId).subscribe(() => {
      const updatedAppointments = this.appointments.filter(appointments => appointments.id !== appointmentId);
      this.appointments = updatedAppointments;
      this.appointmentsUpdated.next([...this.appointments]);
    })
  }

  formatDate(date: object) {
    let appointmentDate = moment(date).format('dddd - Do MMMM YYYY - HH:mm');
    return appointmentDate;
  }

  combineDateTime(datePick: Date, timePick: Date) {
    var date = datePick;
    var time = timePick;
    var datetime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
                        time.getHours(), time.getMinutes(), time.getSeconds());
    return datetime;
  }

}
