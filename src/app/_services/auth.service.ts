import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AuthData } from '@app/_models/auth';
import { User } from '@app/_models/user';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/internal/operators/map';

const BACKEND_URL = environment.apiUrl + '/user/';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private isAuthenticated = false;
  private isAdminUser = false;
  private isMedicUser = false;
  private token: string;
  private authStatusListener = new Subject<boolean>();
  private adminRoleListener = new Subject<boolean>();
  private medicRoleListener = new Subject<boolean>();
  private medicsListener = new Subject<object>();
  private tokenTimer: NodeJS.Timer;
  private userId: string;
  private role: string;
  private medics: User[] = [];
  // private medicsUpdated = new Subject<User[]>();


  constructor(private http: HttpClient, private router: Router, private _snackBar: MatSnackBar) {}

  getToken() {
    return this.token;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  getAdminRoleListener() {
    return this.adminRoleListener.asObservable();
  }

  getMedicRoleListener() {
    return this.medicRoleListener.asObservable();
  }

  getMedicUpdateListener() {
    return this.medicsListener.asObservable();
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getIsAdminUser() {
    return this.isAdminUser;
  }

  getIsMedicUser() {
    return this.isMedicUser;
  }

  getUserId() {
    return this.userId;
  }

  getMedicList() {
    return this.medics;
  }

  createUser(firstName: string, lastName: string, cnp: number, birthdate: Date, email: string, password: string) {
    const authData: User = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      cnp: cnp,
      birthDate: birthdate,
      role: null
    }

    this.http.post<{message: any}>(BACKEND_URL + 'signup', authData).subscribe(response => {
      console.log(response.message);
      this._snackBar.open(response.message, 'Close', {duration: 5*1000} );

    });
  }

  loginUser(email: string, password: string) {
    const authData: AuthData = { email: email, password: password }

    this.http.post<{token: string, expiresIn: number, userId: string, role: string }>(BACKEND_URL + 'login', authData).subscribe(response => {
      const token = response.token;
      this.token = token;
      if (token) {
        const expireInDuration = response.expiresIn;
        this.setAuthTimer(expireInDuration);
        this.isAuthenticated = true;
        this.authStatusListener.next(true);
        this.userId = response.userId;
        if (response.role == 'admin') {
          this.adminRoleListener.next(true);
        }
        if (response.role == 'medic') {
          this.medicRoleListener.next(true);
        }
        this.role = response.role;
        const now = new Date();
        const expirationDate = new Date(now.getTime() + expireInDuration * 1000);
        this.saveAuthData(token, expirationDate, this.userId, this.role);
        this.router.navigate(['/']);
      }
    });
  }

  getMedics(userID: string) {
    this.http.get<{message: string, medicList: any}>(BACKEND_URL + 'medic-list/' + userID)
      .pipe(map((medicData) => {
        return medicData.medicList.map(medic => {
          return {
            firstName: medic.firstName,
            lastName: medic.lastName,
            id: medic._id,
          }
        })
      }))
      .subscribe((transformedMedicList) => {
        this.medics = transformedMedicList;
        this.medicsListener.next(this.medics);
      });
  }

  setAuthTimer(expireInDuration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, expireInDuration * 1000);
  }

  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    const now = new Date();
    const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.userId = authInformation.userId;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
      if(authInformation.userRole == 'admin') {
        this.isAdminUser = true;
        this.adminRoleListener.next(true);
      }
      if(authInformation.userRole == 'medic') {
        this.isMedicUser = true;
        this.medicRoleListener.next(true);
      }
    }
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.adminRoleListener.next(false);
    this.medicRoleListener.next(false);
    this.userId = null;
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/']);
  }

  private saveAuthData(token: string, expirationDate: Date, userId: string, role: string) {
    localStorage.setItem("token", token);
    localStorage.setItem("expiration", expirationDate.toISOString());
    localStorage.setItem("userId", userId);
    localStorage.setItem("userRole", this.role);
  }

  clearAuthData() {
    localStorage.removeItem("token");
    localStorage.removeItem("expiration");
    localStorage.removeItem("userId");
    localStorage.removeItem("userRole");
  }

  getAuthData() {
    if (!localStorage) {
      return;
    }
    const token = localStorage.getItem("token");
    const expirationDate = localStorage.getItem("expiration");
    const userId = localStorage.getItem("userId");
    const userRole = localStorage.getItem("userRole");
    if (!token || !expirationDate) {
      return;
    }
    return {token: token, expirationDate: new Date(expirationDate), userId: userId, userRole: userRole};
  }
}
