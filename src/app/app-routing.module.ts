import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_guards';
import { AppointmentsComponent } from './appointments/appointments/appointments.component';
import { UserProfileComponent } from './profiles/user-profile/user-profile.component';
import { AdminComponent } from './admin/admin.component';
import { ProfileComponent } from './profiles/profile/profile.component';
import { AccountComponent } from './profiles/account/account.component';
import { MedAppointmentComponent } from './med-appointment/med-appointment.component';
import { RoleGuard } from './_guards/role.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'appointments', component: AppointmentsComponent, canActivate: [AuthGuard] },
  { path: 'med-appointments', component: MedAppointmentComponent, canActivate: [AuthGuard, RoleGuard], data: {expectedRole: 'medic'} },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard, RoleGuard], data: {expectedRole: 'admin'} },
  { path: 'manage-account', component: UserProfileComponent, canActivate: [AuthGuard], children: [
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'account', component: AccountComponent, canActivate: [AuthGuard] }
  ] },

  //lazing loading
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
