import { Component } from "@angular/core";
import { AccountService } from '@app/_services/account.service';
import { AuthService } from '@app/_services/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent {

  constructor(private accountService: AccountService, private authService: AuthService) {}

  onGrant(cnp: number) {
    if (cnp == null || isNaN(cnp)) {
      return;
    }
    const userID = this.authService.getUserId();
    this.accountService.grandMedicAccess(userID, cnp);
  }
}
