import { Component, OnInit } from "@angular/core";
import { NgForm } from '@angular/forms';
import { format } from 'path';
import { AccountService } from '@app/_services/account.service';
import { AuthService } from '@app/_services/auth.service';
import { User } from '@app/_models/user';
import { Subscription } from "rxjs";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit{
  constructor(private accountService: AccountService, private authService: AuthService) {}

  profileInfo: User[] = [];
  private profileSub: Subscription;

  ngOnInit() {
    const userID = this.authService.getUserId();
    this.accountService.getProfileInfo(userID);
    this.profileSub = this.accountService.getProfileUpdateListener()
      .subscribe((profile: User[]) => {
        this.profileInfo = profile;
      });
  }

  onSubmit(form: NgForm) {
    const userID = this.authService.getUserId();
    this.accountService.updateProfileInfo(userID, form.value.firstName, form.value.lastName);
  }
}
