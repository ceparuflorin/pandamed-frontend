import { Component } from "@angular/core";
import { NgForm } from '@angular/forms';
import { AuthService } from '@app/_services/auth.service';
import { AccountService } from '@app/_services/account.service';
import { ConfirmUserDeleteComponent } from '@app/dialogs/confirm-user-delete/confirm-user-delete.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent {

  constructor(private authService: AuthService, private accountService: AccountService, public dialog: MatDialog) {}

  onSave(email: string) {
    console.log(email);
    const userID = this.authService.getUserId();
    this.accountService.updateEmailAdress(userID, email);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmUserDeleteComponent, {
      width: '400px',
    });
  }
}
