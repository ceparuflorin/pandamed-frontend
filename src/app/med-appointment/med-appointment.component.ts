import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Subscription } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';

import { AppointmentService } from '@app/_services/appointment.service';
import { Appointment } from '@app/_models/appointment';
import { ConfirmAppointmentComponent } from '@app/dialogs/confirm-appointment/confirm-appointment.component';
import { CancelAppointmentComponent } from '@app/dialogs/cancel-appointment/cancel-appointment.component';

@Component({
  templateUrl: './med-appointment.component.html',
  styleUrls: ['./med-appointment.component.css']
})

export class MedAppointmentComponent implements OnInit, OnDestroy {

  constructor(public appointmentService: AppointmentService, public dialog: MatDialog) {}

  private medAppointmentSub: Subscription;
  medAppointments: Appointment[] = [];

  displayedColumns: string[] = ['hospital', 'department', 'appointmentDate', 'appointmentStatus', 'actions'];
  dataSource;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.appointmentService.getMedAppointments();
    this.medAppointmentSub = this.appointmentService.getMedAppointmentUpdateListener()
      .subscribe((appointments: Appointment[]) => {
        this.medAppointments = appointments;
        this.dataSource = new MatTableDataSource(this.medAppointments);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        console.log(this.medAppointments);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onConfirm(id: string, appointmentDate: Date): void {
    const dialogRef = this.dialog.open(ConfirmAppointmentComponent, {
      width: '400px',
      data: {id: id, appointmentDate: appointmentDate}
    });
  }

  onCancel(appointmentId: string) {
    const dialogRef = this.dialog.open(CancelAppointmentComponent, {
      width: '400px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.appointmentService.cancelAppointment(appointmentId);
      }
    })
  }

  getColor(appointmentStatus: string) {
    let color: string;
    if (appointmentStatus == 'Pending') {
      color = '#ffe082';
    } else if (appointmentStatus == 'Confirmed') {
      color = '#9ccc65';
    } else {
      color = '#bdbdbd';
    }
    return color;
  }

  ngOnDestroy() {
    this.medAppointmentSub.unsubscribe();
  }
}
