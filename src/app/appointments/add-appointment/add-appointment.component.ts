import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { AppointmentService } from '@app/_services/appointment.service';
import { AuthService } from '@app/_services/auth.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { DateErrorComponent } from '@app/dialogs/date-error/date-error.component';

// import { mimeType } from './mime-type.validator';

@Component({
  selector: 'app-add-appointment',
  templateUrl: './add-appointment.component.html',
  styleUrls: ['./add-appointment.component.css']
})

export class AddAppointmentComponent implements OnInit, OnDestroy {

  constructor(public appointmentService: AppointmentService, private authService: AuthService, public dialog: MatDialog) {}

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  private medicSub: Subscription;
  form: FormGroup;
  imagePreview: string;
  medicsList;
  userId: string;
  appointmentDate: Date = new Date(Date.now());
  appointmentTime: Date = new Date(Date.now());
  min: string = "08:00";
  max: string = "18:00";

  selectedHospital = '';
  selectedDepartment = '';
  hospitals = [
    { value: 'sp-bag', viewValue: "Spitalul Bagdasar-Arseni" },
    { value: 'sp-mil', viewValue: "Spitalul Militar" },
    { value: 'sp-flr', viewValue: "Spitalul Floreasca"}
  ];

  departments = [
    { value: 'dep-chirurgie', viewValue: "Chirurgie" },
    { value: 'dep-imagistica', viewValue: "Imagistica" },
    { value: 'dep-biochim', viewValue: "Biochimie"}
  ];

  ngOnInit() {
    //Set time to 8:00 AM
    this.appointmentTime.setTime(21600*1000);
    this.userId = this.authService.getUserId();
    this.authService.getMedics(this.userId);
    this.medicSub = this.authService.getMedicUpdateListener()
      .subscribe((medics) => {
        this.medicsList = medics;
      });

    this.form = new FormGroup({
      'hospital': new FormControl(null, {validators: [Validators.required]}),
      'department': new FormControl(null, {validators: [Validators.required]}),
      'medic': new FormControl(null, {validators: [Validators.required]}),
      'appointmentDate': new FormControl(this.appointmentDate, {validators: [Validators.required]}),
      'appointmentTime': new FormControl(this.appointmentTime, {validators: [Validators.required]}),
      // 'image': new FormControl(null, {validators: [Validators.required], asyncValidators: [mimeType]}),
      'additionalDetails': new FormControl(null, {validators: [Validators.required, Validators.minLength(5)]})
    });
  }

  onAddAppointment() {
    if (this.form.invalid) {
      return;
    }

    for (let x of this.hospitals) {
      if (x.value === this.form.value.hospital){
        this.selectedHospital = x.viewValue;
      }
    }

    for (let x of this.departments) {
      if (x.value === this.form.value.department){
        this.selectedDepartment = x.viewValue;
      }
    }

    var date = this.form.value.appointmentDate;
    var time = this.form.value.appointmentTime;
    var currentDate: Date = new Date(Date.now());

    var datetime = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
                        time.getHours(), time.getMinutes(), time.getSeconds());

    if(datetime < currentDate) {
      const dialogRef = this.dialog.open(DateErrorComponent, {
        width: '400px',
        //data: {id: id, appointmentDate: appointmentDate}
      });
      return;
    }

    this.appointmentService.addAppointments(this.selectedHospital, this.selectedDepartment, this.form.value.medic, datetime, this.form.value.additionalDetails);
    setTimeout(() => this.formGroupDirective.resetForm(), 0)
  }

  // onFilePicked(event: Event) {
  //   const file = (event.target as HTMLInputElement).files[0];
  //   this.form.patchValue({image: file});
  //   this.form.get('image').updateValueAndValidity();
  //   const reader = new FileReader;
  //   reader.onload = () => {
  //     this.imagePreview = reader.result as string;
  //   }
  //   reader.readAsDataURL(file);
  // }

  ngOnDestroy() {
    this.medicSub.unsubscribe();
  }
}
