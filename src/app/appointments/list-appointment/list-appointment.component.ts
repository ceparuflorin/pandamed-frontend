import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

import { Appointment } from '../../_models/appointment';
import { AppointmentService } from '@app/_services/appointment.service';
import { AuthService } from '@app/_services/auth.service';

@Component({
  selector: 'app-list-appointment',
  templateUrl: './list-appointment.component.html',
  styleUrls: ['./list-appointment.component.css']
})

export class ListAppointmentComponent implements OnInit, OnDestroy {

  constructor(public appointmentService: AppointmentService, private authService: AuthService) {}

  app;
  resAppointment = false;
  appointments: Appointment[] = [];
  userIsAuthenticated = false;
  userId: string;
  datePick: Date = new Date(Date.now());
  timePick: Date = new Date(Date.now());
  private appointmentSub: Subscription;
  private authStatusSub: Subscription;

  ngOnInit() {
    this.appointmentService.getAppointments();
    this.userId = this.authService.getUserId();
    this.appointmentSub = this.appointmentService.getAppointmentUpdateListener()
      .subscribe((appointments: Appointment[]) => {
        this.appointments = appointments;
      });
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authStatusSub = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });
  }

  onDelete(appointmentId: string) {
    this.appointmentService.deleteAppointment(appointmentId);
  }

  toggleReschedule(appointment: Appointment) {
    this.resAppointment = !this.resAppointment;
    this.app = appointment;
  }

  ngOnDestroy() {
    this.appointmentSub.unsubscribe();
    this.authStatusSub.unsubscribe();
  }

  onCancel() {
    this.resAppointment = !this.resAppointment;
  }

  onUpdate(form: NgForm, id: string) {
    var datetime = this.appointmentService.combineDateTime(this.datePick, this.timePick)
    this.appointmentService.rescheduleAppointment(id, datetime);
    this.resAppointment = !this.resAppointment;
  }

  getColor(appointmentStatus: string) {
    let color: string;
    if (appointmentStatus == 'Pending') {
      color = '#ffe082';
    } else if (appointmentStatus == 'Confirmed') {
      color = '#9ccc65';
    } else {
      color = '#bdbdbd';
    }
    return color;
  }
}
