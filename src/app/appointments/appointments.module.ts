import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '@app/angular-material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IgxTimePickerModule, IgxDatePickerModule } from 'igniteui-angular';

import { AppointmentsComponent } from './appointments/appointments.component';
import { AddAppointmentComponent } from './add-appointment/add-appointment.component';
import { ListAppointmentComponent } from './list-appointment/list-appointment.component';

@NgModule({
  declarations: [
    AppointmentsComponent,
    AddAppointmentComponent,
    ListAppointmentComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    IgxTimePickerModule,
    IgxDatePickerModule
  ]
})
export class AppointmentsModule {}
