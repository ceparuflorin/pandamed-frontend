import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import { AppointmentsModule } from './appointments/appointments.module';
import { AuthInterceptor } from './auth/auth-interceptor';
import { ErrorInterceptor } from './error-interceptor';
import { UserProfileComponent } from './profiles/user-profile/user-profile.component';
import { AdminComponent } from './admin/admin.component';
import { AccountComponent } from './profiles/account/account.component';
import { ProfileComponent } from './profiles/profile/profile.component';
import { MedAppointmentComponent } from './med-appointment/med-appointment.component';
import { ConfirmAppointmentComponent } from './dialogs/confirm-appointment/confirm-appointment.component';
import { ConfirmUserDeleteComponent } from './dialogs/confirm-user-delete/confirm-user-delete.component';
import { CancelAppointmentComponent } from './dialogs/cancel-appointment/cancel-appointment.component';
import { DateErrorComponent } from './dialogs/date-error/date-error.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    UserProfileComponent,
    AdminComponent,
    AccountComponent,
    ProfileComponent,
    MedAppointmentComponent,
    ConfirmAppointmentComponent,
    ConfirmUserDeleteComponent,
    CancelAppointmentComponent,
    DateErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HammerModule,
    AngularMaterialModule,
    AppointmentsModule,
    FormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
