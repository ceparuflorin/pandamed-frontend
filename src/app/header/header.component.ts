import { Component, OnInit, OnDestroy } from '@angular/core';

import { AuthService } from '@app/_services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit, OnDestroy {
  userIsAuthenticated = false;
  adminRole = false;
  medicRole = false;
  private authListenerSubs: Subscription;
  private adminRoleSubs: Subscription;
  private medicRoleSubs: Subscription;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

    this.adminRole = this.authService.getIsAdminUser();
    this.adminRoleSubs = this.authService.getAdminRoleListener()
      .subscribe(isAdmin => {
        this.adminRole = isAdmin;
      })

    this.medicRole = this.authService.getIsMedicUser();
    this.medicRoleSubs = this.authService.getMedicRoleListener()
      .subscribe(isMedic => {
        this.medicRole = isMedic;
      })
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
    this.adminRoleSubs.unsubscribe();
    this.medicRoleSubs.unsubscribe();
  }
}
