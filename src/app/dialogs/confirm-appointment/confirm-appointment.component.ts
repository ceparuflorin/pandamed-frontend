import { Component, Inject } from "@angular/core";
import { MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AppointmentService } from '@app/_services/appointment.service';
import { Router } from '@angular/router';
import { ResourceLoader } from '@angular/compiler';

@Component({
  selector: 'app-confirm-appointment',
  templateUrl: './confirm-appointment.component.html',
  styleUrls: ['./confirm-appointment.component.css']
})

export class ConfirmAppointmentComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data, public appointmentService: AppointmentService, private router: Router) {}

  onConfirm(appointmentId: string) {
    this.appointmentService.confirmAppointment(appointmentId);
    window.location.reload();
  }
}
