import { Component } from "@angular/core";
import { AuthService } from '@app/_services/auth.service';
import { AccountService } from '@app/_services/account.service';

@Component({
  templateUrl: './cancel-appointment.component.html',
  styleUrls: ['./cancel-appointment.component.css']
})
export class CancelAppointmentComponent {

  constructor(private authService: AuthService, private accountService: AccountService) {}

  // onDelete() {
  //   const userID = this.authService.getUserId();
  //   this.accountService.deleteUser(userID);
  //   window.location.reload();
  // }
}
