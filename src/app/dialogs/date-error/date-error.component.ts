import { Component, Inject } from "@angular/core";
import { MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-date-error',
  templateUrl: './date-error.component.html',
  styleUrls: ['./date-error.component.css']
})

export class DateErrorComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data) {}

}
