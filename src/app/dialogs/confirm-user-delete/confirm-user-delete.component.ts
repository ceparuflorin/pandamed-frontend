import { Component } from "@angular/core";
import { AuthService } from '@app/_services/auth.service';
import { AccountService } from '@app/_services/account.service';

@Component({
  templateUrl: './confirm-user-delete.component.html',
  styleUrls: ['./confirm-user-delete.component.css']
})
export class ConfirmUserDeleteComponent {

  constructor(private authService: AuthService, private accountService: AccountService) {}

  onDelete() {
    const userID = this.authService.getUserId();
    this.accountService.deleteUser(userID);
    window.location.reload();
  }
}
