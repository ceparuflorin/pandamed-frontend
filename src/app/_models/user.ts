﻿export interface User {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  birthDate: Date;
  cnp: number,
  role: string
}
