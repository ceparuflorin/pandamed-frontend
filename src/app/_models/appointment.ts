export interface Appointment {
  id: string,
  hospital: string,
  department: string,
  medicId: string,
  appointmentDate: Date,
  additionalDetails: string,
  patientId: string,
  appointmentStatus: string
}
