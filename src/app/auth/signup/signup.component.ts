﻿import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '@app/_services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: 'signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent {

  constructor(public AuthService: AuthService) {}

  flipped:boolean = false;

  onSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.AuthService.createUser(form.value.firstName, form.value.lastName, form.value.cnp, form.value.birthDate, form.value.email, form.value.password);
    form.resetForm();
    this.flipped = false;
  }

}
