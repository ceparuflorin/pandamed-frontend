﻿import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '@app/_services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})

export class LoginComponent {

  constructor(public AuthService: AuthService) {}

  onLogin(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.AuthService.loginUser(form.value.email, form.value.password);
    form.resetForm();
  }
}
