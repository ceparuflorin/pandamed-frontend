import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { SignupComponent } from '../signup/signup.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})

export class AuthComponent implements AfterViewInit {

  @ViewChild(SignupComponent) child;

  flipped = false;

  flipIt() {
    this.flipped = !this.flipped;
  }

  ngAfterViewInit() {
    this.flipped = this.child.flipped;
  }
}
